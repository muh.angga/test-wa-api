import App from "./App";

async function main() {
    const app = new App()
    await app.connectDatabase()
    await app.recoverWhatsAppClient()
    app.listen()
}

main()