import fs from 'fs/promises'

type tdb = {
    data: DataDB
}

type DataDB = {
    client: string[],
    messages: Message[],
}

type Message = {
    from: string;
    to: string;
    message: string;
    timestamp: string;
}

const db: tdb = {
    data: {
        client: [],
        messages: [],
    }
}

export const saveDB = () => {
    fs.writeFile(process.cwd() + '/db.json', JSON.stringify(db.data, null, '\t'))
}

export const loadDB = async () => {
    const text = await fs.readFile(process.cwd() + '/db.json')
    try {
        db.data = JSON.parse(text.toString())
    } catch (err) {}
}

export default db