import DBConfig from "./../../config/db.config";
import { Sequelize } from 'sequelize';

const DbConnect = new Sequelize({
    dialect: "postgres",
    host: DBConfig.postgres.DB_HOST,
    port: DBConfig.postgres.DB_PORT,
    database: DBConfig.postgres.DB_DATABASE,
    username: DBConfig.postgres.DB_USERNAME,
    password: DBConfig.postgres.DB_PASSWORD,
    logging: DBConfig.postgres.DB_LOGGING
});
export default DbConnect;