import express from "express";
import cors from "cors";
import helmet from "helmet";
import fileupload from "express-fileupload";
import SequelizeConnection from "./libs/Sequlize/Sequelize.connection";
import AppConfig from './config/app.config'
import Logger from "./libs/logger/Winston";
import routeWhatsApp from "./WhatsApp/WhatsApp.Route";
import { Server } from 'socket.io'
import db, { loadDB, saveDB } from "./libs/db-file/lowdb";
import WhatsAppClientService from "./WhatsApp/WhatsappClient.Service";
import _ from 'lodash'

class App {
    public app: express.Application;
    public port: number;
    public socketIO: Server;

    constructor(){
        this.app    = express();
        this.port   = AppConfig.app.PORT;
        this.app.use(helmet());
        this.app.use(cors());
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: true }));
        this.app.use(fileupload({
            useTempFiles: true,
            tempFileDir: `${process.cwd()}/tmp`,
            limits: { fileSize: 2 * 1024 * 1024 },
        }))

        const http = require('http');
        const server = http.createServer(this.app);
        this.socketIO = new Server(server);

        this.app.use('/api/whatsapp', routeWhatsApp)
    }
    
    public listen() {
        this.app.listen(this.port, () => {
            Logger.info(`App listening on the port ${this.port}`);
        });
    }

    public async connectDatabase(){
        // SequelizeConnection.sync().then(function(){
        //     Logger.info('DB connection successful.');
        // }, function(err){
        //     Logger.error('DB connection unsuccessful.');
        //     Logger.error(err)
        //     process.exit(1);
        // });

        await loadDB()
        if (db.data == null) db.data ||= {
            client: [],
            messages: [],
        }

        setInterval(async () => {
            await saveDB()
        }, 15_000)
    }

    public implementSocketIO(){
        this.socketIO.on('connection', (socket) => {
            console.log(`${socket.id} user connected`);
        });
    }

    public async recoverWhatsAppClient() {
        _.map(db.data?.client, client => {
            WhatsAppClientService.getInstance(client)
        })
    }
}

export default App;