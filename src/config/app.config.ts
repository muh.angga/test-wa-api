import * as dotenv from "dotenv";

dotenv.config()

export const PORT = Number(process.env.PORT)

const config = {
    app: {
        PORT,
    }
}

export default config
