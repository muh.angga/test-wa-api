import * as dotenv from "dotenv";

dotenv.config()

export const DB_USERNAME = String(process.env.DB_USERNAME)
export const DB_PASSWORD = String(process.env.DB_PASSWORD)
export const DB_HOST = String(process.env.DB_HOST)
export const DB_PORT = Number(process.env.DB_PORT)
export const DB_DATABASE = String(process.env.DB_DATABASE)
export const DB_LOGGING = Boolean(process.env.DB_LOGGING)

const config = {
    postgres: {
        DB_USERNAME,
        DB_PASSWORD,
        DB_HOST,
        DB_PORT,
        DB_DATABASE,
        DB_LOGGING
    }
}

export default config