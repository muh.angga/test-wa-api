import { Router } from "express";
import WhatsAppController from "./WhatsApp.Controller";


const whatsAppController = new WhatsAppController()

const route = Router()

route.get('/', whatsAppController.list)
route.post('/', whatsAppController.create)
route.get('/message', whatsAppController.listMessage)
route.get('/:id', whatsAppController.info)
route.get('/:id/info/:phone', whatsAppController.infoUser)
route.post('/:id/message/send', whatsAppController.sendMessage)

export default route