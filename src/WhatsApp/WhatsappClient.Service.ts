import makeWASocket, * as WABailyes from "@adiwajshing/baileys";
import { AnyMessageContent, delay, DisconnectReason, isJidBroadcast, makeCacheableSignalKeyStore, makeInMemoryStore, MessageRetryMap,  } from "@adiwajshing/baileys";
import generateClientName from 'project-name-generator';
import { cli } from "winston/lib/winston/config";
import db from "../libs/db-file/lowdb";
import qrcode from 'qrcode'
import _ from 'lodash'

const pathFileAuth = process.cwd() + '/tmp/auth'

export type ListInstanceWhatsAppClient = {
    [key: string]: WhatsAppClientService
}

export default class WhatsAppClientService {

    static _instances: ListInstanceWhatsAppClient = {};

    uuid: string | null = null;
    client: WABailyes.WASocket | null = null;
    msgCounterRertry: MessageRetryMap = {};
    qr: string = "";

    constructor() {}

    async intiateClient() {
        const { state, saveCreds } = await WABailyes.useMultiFileAuthState(pathFileAuth +'/'+this.uuid!)

        // fetch latest version of WA Web
        const { version, isLatest } = await WABailyes.fetchLatestBaileysVersion()
        console.log(`using WA v${version.join('.')}, isLatest: ${isLatest}`)
    
        this.client = makeWASocket({
            version,
            printQRInTerminal: false,
            auth: {
                creds: state.creds,
                keys: state.keys
            },
            msgRetryCounterMap: this.msgCounterRertry,
        })

        this.client!.ev.process(
            // events is a map for event name => event data
            async(events) => {
                // something about the connection changed
                // maybe it closed, or we received all offline message or connection opened
                if(events['connection.update']) {
                    const update = events['connection.update']
                    const { connection, lastDisconnect, qr, isNewLogin, isOnline } = update
                    if(connection === 'close') {
                        // reconnect if not logged out
                        if((lastDisconnect?.error)) {
                            console.log(lastDisconnect?.error.message)
                            await this.intiateClient()
                        } else {
                            console.log('Connection closed. You are logged out.')
                        }
                    } else if (connection == 'open') {
                    }
                    
                    if (qr) {
                        this.qr = qr;
                    } 
                    console.log('connection update', update, isNewLogin, isOnline)
                }
    
                // credentials updated -- save them
                if(events['creds.update']) {
                    await saveCreds()
                }
    
                if(events.call) {
                    console.log('recv call event', events.call)
                }
    
                // received a new message
                if(events['messages.upsert']) {
                    const upsert = events['messages.upsert']
                    console.log('recv messages ', JSON.stringify(upsert, undefined, 2))
    
                    if(upsert.type === 'notify') {
                        for(const msg of upsert.messages) {
                            if(!msg.key.fromMe && true && msg.key.remoteJid?.includes('s.whatsapp.net')) {
                                console.log('replying to', msg.key.remoteJid)
                                db.data.messages.push({
                                    from: msg.key.remoteJid!.split('@')[0],
                                    to: this.uuid!,
                                    message: msg.message?.conversation || msg.message?.extendedTextMessage?.text || "",
                                    timestamp: msg.messageTimestamp,
                                })
                                await this.client?.readMessages([msg.key])
                            }
                        }
                    }
                }
    
                // messages updated like status delivered, message deleted etc.
                if(events['messages.update']) {
                    console.log('message-update',events['messages.update'])
                }
    
                if(events['message-receipt.update']) {
                    console.log('message-receipt', events['message-receipt.update'])
                }
    
                if(events['messages.reaction']) {
                    console.log('message-reaction',events['messages.reaction'])
                }
    
                if(events['presence.update']) {
                    console.log('presence-update',events['presence.update'])
                }
    
                if(events['chats.update']) {
                    console.log('chats.update',events['chats.update'])
                }
    
                if(events['contacts.update']) {
                    for(const contact of events['contacts.update']) {
                        if(typeof contact.imgUrl !== 'undefined') {
                            const newUrl = contact.imgUrl === null
                                ? null
                                : await this.client!.profilePictureUrl(contact.id!).catch(() => null)
                            console.log(
                                `contact ${contact.id} has a new profile pic: ${newUrl}`,
                            )
                        }
                    }
                }
    
                if(events['chats.delete']) {
                    console.log('chats deleted ', events['chats.delete'])
                }
            }
        )

        // wait 3s for qr
        await new Promise<void>((resolve) => {
            setTimeout(() => resolve(), 3000)
        })

        if (this.qr) {
            this.createQRImage(this.qr)
        }

        return this.client!;
    }

    async sendMessage(phone: string, messsage: string) {
        const id = `${phone}@@s.whatsapp.net`
        await this.client?.sendMessage(id, { text: messsage })
    }

    async getContact(phone: string) {
        return await this.client?.fetchStatus(`${phone}@s.whatsapp.net`)
    }

    setUUID(uuid: string) {
        this.uuid = uuid;
    }

    createQRImage(qr: string) {
        qrcode.toFile(process.cwd() + '/tmp/qr/' + this.uuid + '.png', qr)
    }

    static async getInstance(token: string | null = null) {
        if (token == null) {
            token = generateClientName({words: 2, number: true}).dashed;
        }

        if (WhatsAppClientService._instances[token]) {
            const client = WhatsAppClientService._instances[token]
            return client
        } else {
            WhatsAppClientService._instances[token] = new WhatsAppClientService()
            const client = WhatsAppClientService._instances[token]
            client.setUUID(token)
            await client.intiateClient()
            if ((_.find(db.data.client, token)) == null)
                db.data?.client.push(token)
            return client
        }
    }

    static getAllIDInstance() {
        return Object.keys(WhatsAppClientService._instances)
    }

    

}
