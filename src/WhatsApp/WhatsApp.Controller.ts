import * as express from 'express';
import db from '../libs/db-file/lowdb';
import WhatsAppClientService from './WhatsappClient.Service';
import _ from 'lodash';
export default class WhatsAppController {

    constructor() {}

    list = (req: express.Request, res: express.Response) => {
        res.json({
            status: "SUCCESS",
            data: WhatsAppClientService.getAllIDInstance(),
            message: "Success Retrive Data",
        })
    }

    listMessage = (req: express.Request, res: express.Response) => {
        res.json({
            status: "SUCCESS",
            data: _(db.data?.messages).chain().sortBy(x => x.timestamp).reverse().value(),
            message: "Success Retrive Data",
        })
    }

    create = async (req: express.Request, res: express.Response) => {
        const code = req.body.code 
        const waClient = await WhatsAppClientService.getInstance(code)
        res.json({
            status: "SUCCESS",
            data: {
                uuid: waClient.uuid,
                qr: waClient.qr,
            },
            message: "Success Retrive Data",
        })
    }

    info = async (req: express.Request, res: express.Response) => {
        const code = req.body.id 
        const waClient = await WhatsAppClientService.getInstance(code)
        res.json({
            status: "SUCCESS",
            data: {
                uuid: waClient.uuid,
                qr: waClient.qr,
            },
            message: "Success Retrive Info",
        })
    }

    infoUser = async (req: express.Request, res: express.Response) => {
        const code = req.params.id 
        const phone = req.params.phone 
        const waClient = await WhatsAppClientService.getInstance(code)
        res.json({
            status: "SUCCESS",
            data: await waClient.getContact(phone),
            message: "Success Retrive Info",
        })
    }

    qr = async (req: express.Request, res: express.Response) => {
        const code = req.body.id 
        const waClient = await WhatsAppClientService.getInstance(code)
        res.json({
            status: "SUCCESS",
            data: {
                uuid: waClient.uuid,
                qr: waClient.qr,
            },
            message: "Success Retrive Info",
        })
    }

    sendMessage = async (req: express.Request, res: express.Response) => {
        const code = req.params.id 
        const { phone, text } = req.body 
        console.log(req.body, code)
        const waClient = await WhatsAppClientService.getInstance(code)
        await waClient.sendMessage(phone, text)
        res.json({
            status: "SUCCESS",
            data: [],
            message: "Success Send Message",
        })
    }

}