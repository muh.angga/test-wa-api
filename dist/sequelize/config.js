require('dotenv').config();

const db_host = process.env.DB_HOST; 
const db_name = process.env.DB_DATABASE;
const db_password = process.env.DB_PASSWORD; 
const db_user = process.env.DB_USERNAME;
const db_dialect = process.env.DB_DIALECT;
const db_port = process.env.DB_PORT;

module.exports = {
  "development": {
    "username": db_user,
    "password": db_password,
    "database": db_name,
    "host": db_host,
    "port": db_port,
    "dialect": db_dialect,
    "logging": console.log
  },
  "test": {
    "username": db_user,
    "password": db_password,
    "database": db_name,
    "host": db_host,
    "port": db_port,
    "dialect": db_dialect,
    "logging": console.log
  },
  "production": {
    "username": db_user,
    "password": db_password,
    "database": db_name,
    "host": db_host,
    "port": db_port,
    "dialect": db_dialect,
    "logging": console.log
  }
}
