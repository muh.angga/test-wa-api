import makeWASocket, * as WABailyes from "@adiwajshing/baileys";
import { AnyMessageContent, delay, DisconnectReason, isJidBroadcast, makeCacheableSignalKeyStore, makeInMemoryStore, MessageRetryMap,  } from "@adiwajshing/baileys";
import { Err } from "joi";
import P from 'pino'
import qrcode from 'qrcode';

const msgRetryCounterMap: MessageRetryMap = { }
const Logger = P({ timestamp: () => `,"time":"${new Date().toJSON()}"` }).child({})
Logger.level = 'trace'
const store = makeInMemoryStore({ logger: Logger })
store?.readFromFile('./baileys_store_multi.json')
// save every 10s
setInterval(() => {
	store?.writeToFile('./baileys_store_multi.json')
}, 10_000)

const main = async () => {

    const { state, saveCreds } = await WABailyes.useMultiFileAuthState('baileys_auth_infox')

    // fetch latest version of WA Web
	const { version, isLatest } = await WABailyes.fetchLatestBaileysVersion()
	console.log(`using WA v${version.join('.')}, isLatest: ${isLatest}`)

    const sock = makeWASocket({
        version,
		logger: Logger,
        printQRInTerminal: false,
        auth: {
            creds: state.creds,
            keys: state.keys
        },
        msgRetryCounterMap,
        // ignore all broadcast messages -- to receive the same
        // comment the line below out
        // shouldIgnoreJid: jid => isJidBroadcast(jid),
        // implement to handle retries
        getMessage: async (key: any) => {
            if (store) {
                // const msg = await store.loadMessage(key.remoteJid!, key.id!, sock);
                // return msg?.message || undefined;
            }
			console.log('get message executed', key)
            // only if store is present
            return {
                conversation: 'hello'
            };
        }
    })

    store?.bind(sock.ev)

    const sendMessageWTyping = async(msg: AnyMessageContent, jid: string) => {
		await sock.presenceSubscribe(jid)
		await delay(500)

		await sock.sendPresenceUpdate('composing', jid)
		await delay(2000)

		await sock.sendPresenceUpdate('paused', jid)

		await sock.sendMessage(jid, msg)
	}

	// the process function lets you process all events that just occurred
	// efficiently in a batch
	sock.ev.process(
		// events is a map for event name => event data
		async(events) => {
			// something about the connection changed
			// maybe it closed, or we received all offline message or connection opened
			if(events['connection.update']) {
				const update = events['connection.update']
				const { connection, lastDisconnect, qr, isNewLogin, isOnline } = update
				if(connection === 'close') {
					// reconnect if not logged out
					if((lastDisconnect?.error)) {
                        console.log(lastDisconnect?.error.message)
						main()
					} else {
						console.log('Connection closed. You are logged out.')
					}
				} else if (connection == 'open') {
				}
				
				if (qr) {
					qrcode.toFile(process.cwd() + '/output/temp-qr.png', qr)
				} 
				console.log('connection update', update, isNewLogin, isOnline)
			}

			// credentials updated -- save them
			if(events['creds.update']) {
				await saveCreds()
			}

			if(events.call) {
				console.log('recv call event', events.call)
			}

			// received a new message
			if(events['messages.upsert']) {
				const upsert = events['messages.upsert']
				console.log('recv messages ', JSON.stringify(upsert, undefined, 2))

				if(upsert.type === 'notify') {
					for(const msg of upsert.messages) {
						if(!msg.key.fromMe && true) {
							console.log('replying to', msg.key.remoteJid)
							await sock!.readMessages([msg.key])
							await sendMessageWTyping({ text: 'Hello there!' }, msg.key.remoteJid!)
						}
					}
				}
			}

			// messages updated like status delivered, message deleted etc.
			if(events['messages.update']) {
				console.log('message-update',events['messages.update'])
			}

			if(events['message-receipt.update']) {
				console.log('message-receipt', events['message-receipt.update'])
			}

			if(events['messages.reaction']) {
				console.log('message-reaction',events['messages.reaction'])
			}

			if(events['presence.update']) {
				console.log('presence-update',events['presence.update'])
			}

			if(events['chats.update']) {
				console.log('chats.update',events['chats.update'])
			}

			if(events['contacts.update']) {
				for(const contact of events['contacts.update']) {
					if(typeof contact.imgUrl !== 'undefined') {
						const newUrl = contact.imgUrl === null
							? null
							: await sock!.profilePictureUrl(contact.id!).catch(() => null)
						console.log(
							`contact ${contact.id} has a new profile pic: ${newUrl}`,
						)
					}
				}
			}

			if(events['chats.delete']) {
				console.log('chats deleted ', events['chats.delete'])
			}
		}
	)
	return sock
}

main()
